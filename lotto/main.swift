//
//  main.swift
//  lotto
//  Created by NTUBIMD on 2017/11/20.
//  Copyright © 2017年 NTUBIMD. All rights reserved.
//

import Foundation

//輸入的func
func input() -> String {
    let keyboard = FileHandle.standardInput
    let inputData = keyboard.availableData
    return NSString(data: inputData, encoding: String.Encoding.utf8.rawValue)! as String
}

//產生中獎號碼
var bingo = Array<Int>()//一個空的陣列
var continuePlay = true

while (continuePlay){
    while(bingo.count < 6){//陣列數是否有６個
        let randomNumber = Int(arc4random_uniform(49) + 1)
        if !bingo.contains(randomNumber){//判斷是否有重複值
            bingo.append(randomNumber)// 陣列存一個新的值
        }
    }
    
    bingo.sort { $0 < $1 }//從小到大排列
    print("請輸入自動０或手動１選號")
    
    var select = input()//輸入自動手動
    if select.contains("0"){
        var autoN = Array<Int>()//一個空的陣列
        while(autoN.count < 6){//陣列數是否有６個
            var randomNumber0 = Int(arc4random_uniform(49) + 1)
            if !autoN.contains(randomNumber0){//判斷是否有重複值
                autoN.append(randomNumber0)// 陣列存一個新的值
            }
        }
        autoN.sort { $0 < $1 }//從小到大排列
        print( "中獎號碼：",bingo)
        print( "選號號碼：",autoN)
        //判斷是否有中獎
        var bingoTot = 0
        for k in 0...5{
            for kk in 0...5{
                if bingo[k] == autoN[kk]{
                    bingoTot += 1
                    print("中獎號碼：",autoN[kk])
                }
            }
        }
        if bingoTot == 0{
            print("未中獎")
        }
    }else if select.contains("1"){
        print("\n請輸入號碼")
        var typeN = Array<String>()
        while(typeN.count < 6){//陣列的數值夠不夠
            let several_1 = input()//type num
            var type = several_1
            type = type.replacingOccurrences(of: "\n", with: "")
            
            //1a1 to int 1  , 11 to int 11 ,  1a;dklfkdl;?11 to int 1 , 11 12 23134453
            
            if Int(type)?.description.count == type.count{
                if Int(type)! <= 49 && Int(type)! > 0 {
                    if !typeN.contains(type){//判斷是否有重複值
                        typeN.append(type)// 陣列存一個新的值
                    }else{
                        print("重複輸入,請重新輸入")
                    }
                }else{
                    print("超出範圍 0<x<=49")
                }
            }else{
                print("錯誤選號","請重新輸入")
            }
        }
        for replac in 0...5{
            typeN[replac] = typeN[replac].replacingOccurrences(of: "\n", with: "")
        }
        let tt = (typeN.map{String($0)}).joined(separator: ",")//int to string
        let bb = (bingo.map{String($0)}).joined(separator: ",")//int to string
        print("中獎號碼",bb)
        print("自選號碼：",tt)
        let ttA = tt.split(separator: ",")
        let bbA = bb.split(separator: ",")
        for a in ttA{
            for b in bbA{
                if a == b{
                    print("中獎號碼",b)
                }
            }
        }
    }else{
        print("請重新輸入０或１")
    }
    
    print("是否繼續遊玩(0為是/其他則為否)")
    var su = true
    while (su){
        let continueP = input()
        if continueP.contains("0"){
            su = false
            continuePlay = true
        }else{
            su = false
            continuePlay = false
        }
    }
}
